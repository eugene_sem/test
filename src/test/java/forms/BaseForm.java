package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

public class BaseForm {

    private By _locator;
    static protected WebDriver webDriver;


    public BaseForm(WebDriver webDriver, By locator)
    {
        this.webDriver=webDriver;
        _locator=locator;
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated((_locator)));
        wait.until(ExpectedConditions.elementToBeClickable(_locator));
    }

}
