package forms;

import org.openqa.selenium.WebDriver;

public class DraftFolder extends MainPage{
    protected final WebDriver driver;
    public DraftFolder(){
        super(webDriver, draftsLocator);
        this.driver = webDriver;
    }
}
