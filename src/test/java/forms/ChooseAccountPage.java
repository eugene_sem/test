package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class ChooseAccountPage extends BaseForm{
    private static final Logger log = Logger.getLogger(ChooseAccountPage.class.getName());
    private static By addAccountLocator = By.id("account-chooser-add-account");
    private By removeAccount = By.id("edit-account-list");
    private By doneButton = By.cssSelector("a[jsname='AHldd']");

    private final WebDriver driver;
    public ChooseAccountPage(){
        super(webDriver, addAccountLocator);
        this.driver = webDriver;
    }
    public EnterEmailPage addAccount(){
        log.info("Add new account");
        driver.findElement(addAccountLocator).click();
        return new EnterEmailPage(driver);
    }
    public ChooseAccountPage editAccountList(){
        log.info("Edit account List");
        driver.findElement(removeAccount).click();
        return this;
    }
    public EnterEmailPage removeAccount(String email){
        log.info("Remove account from the list");
        driver.findElement(By.cssSelector("button[value='"+email+"']")).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(doneButton));
        wait.until(ExpectedConditions.elementToBeClickable(doneButton));
        driver.findElement(doneButton).click();
        return new EnterEmailPage(driver);
    }

}
