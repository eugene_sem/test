package forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import java.util.logging.Logger;


public class EnterEmailPage extends BaseForm {
    private static By emailLocator = By.id("Email");
    private By nextButton = By.id("next");
    private static final Logger log = Logger.getLogger(EnterEmailPage.class.getName());

    private final WebDriver driver;

    public EnterEmailPage(WebDriver driver){
        super(driver, emailLocator);
        this.driver = webDriver;
    }

    public EnterEmailPage typeUsername(String username) {
        log.info("Type username");
        driver.findElement(emailLocator).sendKeys(username);
        return this;
    }

    public EnterPasswordPage submitEmail(){
        log.info("Submit username");
        driver.findElement(emailLocator).submit();
        return new EnterPasswordPage();
    }

}
