package forms;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class MainPage extends BaseForm {
    private static final Logger log = Logger.getLogger(MainPage.class.getName());
    protected static By writeMessageButtonLocator = By.cssSelector("div[gh='cm']"); //div[id=':3z'] > div >
    protected static By inboxLocator = By.xpath("//a[contains(@tabindex, '0')][contains(@href, 'inbox')]");
    protected  By goToInboxLocator = By.xpath("//a[contains(@href, 'inbox')]");
    protected static By draftsLocator = By.xpath("//a[contains(@tabindex, '0')][contains(@href, 'drafts')]");
    protected  By goToDraftsLocator = By.xpath("//a[contains(@href, 'drafts')]");
    protected static By sentLocator = By.xpath("//a[contains(@tabindex, '0')][contains(@href, 'sent')]");
    protected  By goToSentLocator = By.xpath("//a[contains(@href, 'sent')]");
    protected static By userIconLocator = By.xpath("//a[contains(@href, 'SignOutOptions')]");
    protected static By expandMoreLocator = By.cssSelector("span[gh='mll']");
    protected static By trashLocator = By.xpath("//a[contains(@tabindex, '-1')][contains(@href, 'trash')]");
    protected  By goToTrashLocator = By.xpath("//a[contains(@href, 'trash')]");
    protected By searchBoxLocator = By.name("q");
    protected By searchButtonLocator = By.id("gbqfb");
    protected By emailLocator = By.id("reauthEmail");
    protected By loadingLocator = By.cssSelector("span[class='v1']");

    protected final WebDriver driver;

    public MainPage(WebDriver webDriver, By locator)
    {
        super(webDriver, locator);
        this.driver = webDriver;
    }
    public NewDraftEmail clickWriteMessageButton(){
        log.info("Start to create of new email");
        driver.findElement(writeMessageButtonLocator).click();
        return new NewDraftEmail();
    }
    public InboxFolder goToInboxFolder(){
        log.info("Go to the Inbox folder");
        driver.findElement(goToInboxLocator).click();
        return new InboxFolder();
    }
    public DraftFolder goToDraftFolder(){
        log.info("Go to the Draft folder");
        driver.findElement(goToDraftsLocator).click();
        return new DraftFolder();
    }
    public SentFolder goToSentFolder(){
        log.info("Go to the Sent folder");
        driver.findElement(goToSentLocator).click();
        return new SentFolder();
    }
    public RemovedFolder goToRemovedFolder(){
        log.info("Go to the Trash folder");
        expandMoreItems();
        driver.findElement(goToTrashLocator).click();
        return new RemovedFolder();
    }

    public MainPage expandMoreItems(){
        driver.findElement(expandMoreLocator).click();
        return this;
    }
    public boolean isMessageFound(String topic) {
        driver.findElement(By.cssSelector("div[gh='mtb'] > div > div > div > div[act='20']")).click();
        waitLoading();
        WebDriverWait wait = new WebDriverWait(driver, 15);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[text()='" + topic + "']/..")));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public DraftEmail openDraftEmail(String topic){
        log.info("Open the draft email");
        driver.findElement(By.xpath(".//*[text()='" + topic + "']/..")).click();
        return new DraftEmail();
    }
    public EmailPage openEmail(String topic){
        log.info("Open the email");
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[text()='" + topic + "']/..")));
        webDriver.findElement(By.xpath(".//*[text()='" + topic + "']/..")).click();
        return new EmailPage();
    }
    public AccountInformation openAccountInfo(){
        log.info("Open Account Info");
        webDriver.findElement(userIconLocator).click();
        return new AccountInformation();
    }
    public void waitLoading(){
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(loadingLocator));
    }
}
