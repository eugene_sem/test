package tests;
import forms.*;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EmailsTest extends BaseTest {

    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                // fill in test data according to following conditions
                // new Object[] {"user1 login","user1 password","user1 email address","user2 login","user2 password","user2 email address", "email topic", "email content"}
                new Object[]{"es.test.acc1", "qwe123QWE", "es.test.acc1@gmail.com", "es.test.acc2", "qwe123QWE", "es.test.acc2@gmail.com", RandomGenerator.makeRandomString(10), "Email Content"}
        };
    }

    @Test(dataProvider = "testData")
    public void testSaveEmailToDraftsAndSendIt(String user1Login, String user1Password, String user1Email, String user2Login, String user2Password, String user2Email, String topic, String emailContent) {
        driver.get("http://gmail.com/");
        EnterEmailPage emailPage = new EnterEmailPage(driver);
        EnterPasswordPage passwdPage = emailPage.typeUsername(user1Login).submitEmail();
        InboxFolder inbox = passwdPage.typePassword(user1Password).signIn();
        NewDraftEmail newDraftEmail = inbox.clickWriteMessageButton();
        newDraftEmail.enterAddressee(user2Email)
                .enterTopic(topic)
                .enterEmailContent(emailContent)
                .saveAndCloseMessage();
        DraftFolder drafts = inbox.goToDraftFolder();
        Assert.assertTrue(drafts.isMessageFound(topic));
        DraftEmail openedDraft = drafts.openDraftEmail(topic);
        Assert.assertEquals(openedDraft.getAddressee(), user2Email);
        Assert.assertEquals(openedDraft.getEmailTopic(), topic);
        Assert.assertEquals(openedDraft.getEmailContent(), emailContent);
        openedDraft.sendMessage();
        DraftFolder drafts2 = new DraftFolder();
        Assert.assertFalse(drafts2.isMessageFound(topic));
        SentFolder sent = drafts2.goToSentFolder();
        Assert.assertTrue(sent.isMessageFound(topic));
        AccountInformation accountInfo = sent.openAccountInfo();
        EnterPasswordPage passwdPage2 = accountInfo.logOut();
        ChooseAccountPage chooseAccountPage = passwdPage2.signInWithDifferentAccount();
        chooseAccountPage.editAccountList();
        chooseAccountPage.removeAccount(user1Email);
    }

    @Test(dataProvider = "testData")
    public void testSaveEmailToDraftsAndDeleteIt(String user1Login, String user1Password, String user1Email, String user2Login, String user2Password, String user2Email, String topic, String emailContent) {
        driver.get("http://gmail.com/");
        EnterEmailPage emailPage = new EnterEmailPage(driver);
        EnterPasswordPage passwdPage = emailPage.typeUsername(user1Login).submitEmail();
        InboxFolder inbox = passwdPage.typePassword(user1Password).signIn();
        NewDraftEmail newDraftEmail = inbox.clickWriteMessageButton();
        newDraftEmail.enterAddressee(user2Email)
                .enterTopic(topic)
                .enterEmailContent(emailContent)
                .saveAndCloseMessage();
        DraftFolder drafts = inbox.goToDraftFolder();
        Assert.assertTrue(drafts.isMessageFound(topic));
        DraftEmail openedMessage = drafts.openDraftEmail(topic);
        Assert.assertEquals(openedMessage.getAddressee(), user2Email);
        Assert.assertEquals(openedMessage.getEmailTopic(), topic);
        Assert.assertEquals(openedMessage.getEmailContent(), emailContent);
        openedMessage.deleteDraft();
        DraftFolder drafts2 = new DraftFolder();
        Assert.assertFalse(drafts2.isMessageFound(topic));
        AccountInformation accountInfo = drafts2.openAccountInfo();
        EnterPasswordPage passwdPage2 = accountInfo.logOut();
        ChooseAccountPage chooseAccountPage = passwdPage2.signInWithDifferentAccount();
        chooseAccountPage.editAccountList()
                .removeAccount(user1Email);
    }

    @Test(dataProvider = "testData")
    public void testSendEmailAndDeleteIt(String user1Login, String user1Password, String user1Email, String user2Login, String user2Password, String user2Email, String topic, String emailContent) {
        driver.get("http://gmail.com/");
        EnterEmailPage emailPage = new EnterEmailPage(driver);
        EnterPasswordPage passwdPage = emailPage.typeUsername(user1Login).submitEmail();
        InboxFolder inbox = passwdPage.typePassword(user1Password).signIn();
        NewDraftEmail newDraftEmail = inbox.clickWriteMessageButton();
        newDraftEmail.enterAddressee(user2Email)
                .enterTopic(topic)
                .enterEmailContent(emailContent)
                .sendMessage();
        SentFolder sent = inbox.goToSentFolder();
        Assert.assertTrue(sent.isMessageFound(topic));
        EmailPage email = sent.openEmail(topic);
        Assert.assertEquals(email.getEmailFrom(), user1Email);
        Assert.assertEquals(email.getEmailTo(), user2Email);
        Assert.assertEquals(email.getEmailTopic(), topic);
        Assert.assertEquals(email.getEmailContent(), emailContent);
        email.deleteEmail();
        SentFolder sent2 = new SentFolder();
        Assert.assertFalse(sent2.isMessageFound(topic));
        RemovedFolder removed = sent2.goToRemovedFolder();
        Assert.assertTrue(removed.isMessageFound(topic));
        EmailPage email2 = removed.openEmail(topic);
        Assert.assertEquals(email2.getEmailFrom(), user1Email);
        Assert.assertEquals(email2.getEmailTo(), user2Email);
        Assert.assertEquals(email2.getEmailTopic(), topic);
        Assert.assertEquals(email2.getEmailContent(), emailContent);
        email2.confirmEmailDeletion();
        RemovedFolder removed2 = new RemovedFolder();
        Assert.assertFalse(removed2.isMessageFound(topic));
        AccountInformation accountInfo = removed2.openAccountInfo();
        EnterPasswordPage passwdPage2 = accountInfo.logOut();
        ChooseAccountPage chooseAccountPage = passwdPage2.signInWithDifferentAccount();
        chooseAccountPage.editAccountList()
                .removeAccount(user1Email);
    }

    @Test(dataProvider = "testData")
    public void testSendAndReceiveEmail(String user1Login, String user1Password, String user1Email, String user2Login, String user2Password, String user2Email, String topic, String emailContent) {
        driver.get("http://gmail.com/");
        EnterEmailPage emailPage = new EnterEmailPage(driver);
        EnterPasswordPage passwdPage = emailPage.typeUsername(user1Login).submitEmail();
        InboxFolder inbox = passwdPage.typePassword(user1Password).signIn();
        NewDraftEmail newDraftEmail = inbox.clickWriteMessageButton();
        newDraftEmail.enterAddressee(user2Email)
                .enterTopic(topic)
                .enterEmailContent(emailContent)
                .sendMessage();
        SentFolder sent = inbox.goToSentFolder();
        Assert.assertTrue(sent.isMessageFound(topic));
        AccountInformation accountInfo = inbox.openAccountInfo();
        EnterPasswordPage passwdPage2 = accountInfo.logOut();
        ChooseAccountPage chooseAccountPage = passwdPage2.signInWithDifferentAccount();
        chooseAccountPage.editAccountList();
        EnterEmailPage emailPage2 = chooseAccountPage.removeAccount(user1Email);
        EnterPasswordPage passwdPage3 = emailPage2.typeUsername(user2Login).submitEmail();
        InboxFolder inbox2 = passwdPage3.typePassword(user2Password).signIn();
        Assert.assertTrue(inbox2.isMessageFound(topic));
        EmailPage email = inbox2.openEmail(topic);
        Assert.assertEquals(email.getEmailFrom(), user1Email);
        Assert.assertEquals(email.getEmailTo(), user2Email);
        Assert.assertEquals(email.getEmailTopic(), topic);
        Assert.assertEquals(email.getEmailContent(), emailContent);
        InboxFolder inbox3 = email.goToInboxFolder();
        AccountInformation accountInfo2 = inbox3.openAccountInfo();
        EnterPasswordPage passwdPage4 = accountInfo2.logOut();
        ChooseAccountPage chooseAccountPage2 = passwdPage4.signInWithDifferentAccount();
        chooseAccountPage2.editAccountList()
                .removeAccount(user2Email);
    }
}

