package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

/**
 * Created by e.semenyuk on 23.11.2016.
 */
public class BaseTest {
    public static WebDriver driver;
    @BeforeTest
    public void startDriver() {
        System.setProperty("webdriver.gecko.driver", "src/test/java/resources/geckodriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        /*System.setProperty("webdriver.chrome.driver", "src/test/java/resources/chromedriver.exe");
        driver = new ChromeDriver();*/

    }
    @AfterTest
    public void stopDriver() {
        driver.close();
        driver.quit();
    }
}
